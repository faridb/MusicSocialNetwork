package client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class ChatClient {
    private static final String SERVER_ADDRESS = "localhost";
    private static final int SERVER_PORT = 9091;
    
    private final Socket serverSocket;
    private final DataOutputStream out;
    private final DataInputStream in;

    public ChatClient() throws IOException {
        serverSocket = new Socket(SERVER_ADDRESS, SERVER_PORT);
        out = new DataOutputStream(serverSocket.getOutputStream());
        in = new DataInputStream(serverSocket.getInputStream());
    }
    
    public void closeSocket() {
        try {
            serverSocket.close();
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Send the passed String to the Chat Server Socket
     * @param strToSend String to be sent to the chat server
     */
    public void sendString(String strToSend) {
        try {
            out.writeUTF(strToSend);
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Wait to receive a string from the ChatServer
     * @return UTF-8 text received from the chat server
     * @throws IOException 
     */
    public String receiveString() throws IOException {
        return in.readUTF();
    }
    
    /**
     * Send a message to another user 
     * @param toUsername
     * @param message
     */
    public void sendMessage(String toUsername, String message) {
        sendString("SEND MESSAGE\n"
                + toUsername + "\n"
                + message);
    }
    
    /**
     * Send a logout request to the server
     */
    public void logout() {
        sendString("LOGOUT USER");
    }

    /**
     * Send a file to a user
     * @param username
     * @param fileToSend 
     */
    public void sendFile(String username, File fileToSend) {
        sendString("SEND FILE FOR REAL\n"
                + username + "\n"
                + fileToSend.getName() + "\n"
                + fileToSend.length());
        
        try {
            BufferedInputStream bis = new BufferedInputStream(
                    new FileInputStream(fileToSend));
            byte[] data =  new byte[(int)fileToSend.length()];

            bis.read(data);
            out.write(data);
            out.flush();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    /**
     * Receive a file from the Chat Server
     * @param file
     * @param fileLength 
     */
    public void receiveFile(File file, int fileLength) {
        try (BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(file))) {
            byte[] data = new byte[fileLength];

            int remaining = fileLength;
            while(remaining > 0) {
                int read = in.read(data);
                remaining -= read;
                bos.write(data, 0, read);
            }
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
}
