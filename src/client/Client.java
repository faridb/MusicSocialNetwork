package client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import users.User;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class Client {
    private static final String SERVER_ADDRESS = "localhost";
    private static final int SERVER_PORT = 9090;
    
    private final Socket serverSocket;
    private final DataOutputStream out;
    private final DataInputStream in;

    public Client() throws IOException {
        serverSocket = new Socket(SERVER_ADDRESS, SERVER_PORT);
        out = new DataOutputStream(serverSocket.getOutputStream());
        in = new DataInputStream(serverSocket.getInputStream());
    }
    
    /**
     * Send the passed String to the Sever Socket
     * @param strToSend 
     */
    public void sendString(String strToSend) {
        try {
            out.writeUTF(strToSend);
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Wait to receive a string from the Server
     * @return
     * @throws IOException 
     */
    public String receiveString() throws IOException {
        return in.readUTF();
    }
    
    /**
     * Send a request to the server asking to register a new user
     * @param user 
     */
    public void registerNewUser(User user) {
        /* Create a REGISTER NEW USER request which includes the user's info */
        String genresLine = String.join(",", user.getGenres());
        String registerMsg = "REGISTER NEW USER\n"
            + user.getUsername() + "\n"
            + user.getPassword() + "\n"
            + user.getPlaceOfBirth() + "\n"
            + user.getDateOfBirth() + "\n"
            + user.getResidence() + "\n"
            + genresLine;
        
        sendString(registerMsg);
    }
    
    /**
     * Send a request to the server asking to log in a user
     * @param username The user's username
     * @param password The user's password
     */
    public void loginUser(String username, String password) {
        /* Create a LOGIN USER request which includes the user's info */
        String loginRequest = "LOGIN USER\n"
                    + username + "\n"
                    + password;
            
        sendString(loginRequest);
    }
    
    /**
     * Send a logout request to the server
     */
    public void logout() {
        sendString("LOGOUT USER");
    }
    
    /**
     * Send a friendship request to the server containing the current user's
     * username and the potential friend's username
     * @param toUsername
     */
    public void requestFriendship(String toUsername) {
        String request = "FRIENDSHIP REQUEST\n"
                + toUsername + "\n";
        sendString(request);
    }
    
    /**
     * Accept a friendship request
     * @param username Username of the friend
     */
    public void acceptFriendship(String username) {
        String request = "ACCEPT FRIENDSHIP\n"
                + username;
        sendString(request);
    }
    
    /**
     * Refuse a friendship request
     * @param username Username of the user to refuse as a friend
     */
    public void refuseFriendship(String username) {
        String request = "REFUSE FRIENDSHIP\n"
                + username;
        sendString(request);
    }
    
    public void newPost(String post) {
        String request = "NEW POST\n"
                + post;
        sendString(request);
    }
    
    /**
     * Upload a song to the server
     * @param title
     * @param artist
     * @param songFile Song file
     */
    public void uploadSong(String title, String artist, File songFile) {
        // Send information related to the song
        String request = "UPLOAD SONG\n"
                + title + "\n"
                + artist + "\n" 
                + songFile.length();
        sendString(request);
        
        // Send the music file to the server
        try {
            BufferedInputStream bis = new BufferedInputStream(
                    new FileInputStream(songFile));
            byte[] data =  new byte[(int)songFile.length()];

            bis.read(data);
            out.write(data);
            out.flush();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    public void closeSocket() {
        try {
            serverSocket.close();
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    public void playSong(int fileLength, Handler handler) {
        try {
            byte[] data = new byte[fileLength];
            int totalRead = 0;
            int read = 0;
            
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            BufferedOutputStream bos = new BufferedOutputStream(byteStream);
            
            AudioFormat audioFormat = new AudioFormat(44100, 16, 2, true, false);
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat); 
            SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(audioFormat);

            // Receive the audio data from the server
            while(totalRead < fileLength) {
                read = in.read(data);
                bos.write(data, 0, read);
                totalRead += read;
            }
            
            // Play the audio data in a separate thread
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ByteArrayInputStream bbis = new ByteArrayInputStream(byteStream.toByteArray());
                    BufferedInputStream bis = new BufferedInputStream(bbis);
                    int frameSize = line.getFormat().getFrameSize();
                    int read;
                    int totalRead = 0;
                    
                    line.start();
                    try {
                        while (handler.isPlayingSong() && totalRead < fileLength) {
                            read = bis.read(data, 0, 2200);
                            totalRead += read;
                            
                            if (read % frameSize != 0 || read == -1)
                                break;
                            
                            line.write(data, 0, read);
                        }
                    } catch (IOException e) {
                        System.err.println("IOException: " + e.getMessage());
                    }
                    finally {
                        line.close();
                        handler.stopSong();
                    }
                }
            }).start();
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        } catch (LineUnavailableException e) {
            System.err.println("LineUnavailableException: " + e.getMessage());
        }
    }
}
