package client;

import client.gui.ChatScreen;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class ChatHandler implements Runnable {
    private ChatClient chatClient;
    private ChatScreen chatScreen;
    private boolean connected;

    public ChatHandler(ChatClient chatClient, ChatScreen chatScreen) {
        this.chatClient = chatClient;
        this.chatScreen = chatScreen;
        this.connected = true;
    }
    
    public void stopHandler() {
        connected = false;
        chatClient.closeSocket();
    }

    @Override
    public void run() {
        while(connected) {
            try {
                String request = chatClient.receiveString();
                Scanner scanner = new Scanner(request);
                
                if(!scanner.hasNextLine())
                    continue;
                
                String requestType = scanner.nextLine();
                
                switch(requestType) {
                    case "SENT MESSAGE":
                        try {
                            chatScreen.newMessage(scanner.nextLine(),
                                    scanner.nextLine());
                        }
                        catch(NoSuchElementException e) {
                            System.err.println("NoSuchElementException: " + e.getMessage());
                        }
                        break;
                    case "SEND FILE ?":
                        try {
                            chatScreen.askSendFile(scanner.nextLine(),
                                    scanner.nextLine(), scanner.nextLine());
                        }
                        catch(NoSuchElementException e) {
                            System.err.println("NoSuchElementException: " + e.getMessage());
                        }
                        break;
                    case "SEND FILE NO":
                        if(scanner.hasNextLine())
                            chatScreen.refusedFile(scanner.nextLine());
                        break;
                    case "SEND FILE YES":
                        if(scanner.hasNextLine())
                            chatScreen.acceptedFile(scanner.nextLine());
                        break;
                    case "LOGOUT USER":
                        if(scanner.hasNextLine())
                            chatScreen.logout(scanner.nextLine());
                        break;
                    case "RECEIVE SENT FILE":
                        try {
                            chatScreen.receiveFile(scanner.nextLine(),
                                    scanner.nextLine(), scanner.nextLine());
                        }
                        catch(NoSuchElementException e) {
                            System.err.println("NoSuchElementException: " + e.getMessage());
                        }
                        break;
                }
                
            }
            catch(IOException e) {
                connected = false;
            }
        }
    }
}
