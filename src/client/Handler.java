package client;

import client.gui.SocialNetwork;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import users.User;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class Handler implements Runnable {
    private Client client;
    private SocialNetwork socialNetwork;
    private User user;
    private boolean connected;
    private boolean playingSong;

    public Handler(Client client, SocialNetwork socialNetwork, User user) {
        this.client = client;
        this.socialNetwork = socialNetwork;
        this.user = user;
        this.connected = true;
        this.playingSong = false;
        
        // Initialise the list of online users
        getOnlineUsers().forEach((aUser) -> {
            this.socialNetwork.addConnectedUser(aUser);
        });
        
        // Initialise the list of friendship requests
        getFriendshipRequests().forEach((username) -> {
            this.socialNetwork.addFriendshipRequest(username);
        });
        
        // Initialise the friends list
        getFriends().forEach( (username) -> {
            this.socialNetwork.addFriend(username);
        });
        
        // Initialise the posts list
        getPosts().forEach((post) -> {
            this.socialNetwork.addPost(post);
        });
        
        // Initalise the songs list
        getSongs().forEach((song) -> {
            this.socialNetwork.addSong(song);
        });
    }
    
    private HashSet<String> getOnlineUsers() {
        HashSet<String> onlineUsers = new HashSet<>();
        client.sendString("LIST ONLINE USERS");
        
        try {
            String response = client.receiveString();
            Scanner scanner = new Scanner(response);
            
            if(scanner.nextLine().equals("LIST ONLINE USERS")) {
                while(scanner.hasNextLine()) {
                    onlineUsers.add(scanner.nextLine());
                }
            }
            // Remove the current user from the list
            onlineUsers.remove(user.getUsername());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return onlineUsers;
    }
    
    private HashSet<String> getFriendshipRequests() {
        HashSet<String> friendshipRequests = new HashSet<>();
        client.sendString("LIST FRIENDSHIP REQUESTS");
        
        try {
            String response = client.receiveString();
            Scanner scanner = new Scanner(response);
            
            if(scanner.nextLine().equals("LIST FRIENDSHIP REQUESTS")) {
                while(scanner.hasNextLine()) {
                    friendshipRequests.add(scanner.nextLine());
                }
            }
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return friendshipRequests;
    }
    
    private HashSet<String> getFriends() {
        HashSet<String> friends = new HashSet<>();
        client.sendString("LIST FRIENDS");
        
        try {
            String response = client.receiveString();
            Scanner scanner = new Scanner(response);
            
            if(scanner.nextLine().equals("LIST FRIENDS")) {
                while(scanner.hasNextLine()) {
                    friends.add(scanner.nextLine());
                }
            }
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return friends;
    }
    
    private ArrayList<String> getPosts() {
        ArrayList<String> posts = new ArrayList<>();
        client.sendString("LIST POSTS");
        
        try {
            String response = client.receiveString();
            Scanner scanner = new Scanner(response);
            
            if(scanner.nextLine().equals("LIST POSTS")) {
                while(scanner.hasNextLine()) {
                    posts.add(scanner.nextLine());
                }
            }
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return posts;
    }
    
    private ArrayList<String> getSongs() {
        ArrayList<String> songs = new ArrayList<>();
        
        client.sendString("LIST SONGS");
        
        try {
            String response = client.receiveString();
            Scanner scanner = new Scanner(response);
            
            if(scanner.hasNextLine() && 
               scanner.nextLine().equals("LIST SONGS")) {
                while(scanner.hasNextLine()) {
                    songs.add(scanner.nextLine());
                }
            }
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return songs;
    }
    
    public void stopHandler() {
        connected = false;
        playingSong = false;
        client.closeSocket();
    }

    @Override
    public void run() {
        while(connected) {
            try {
                String request = client.receiveString();
                Scanner scanner = new Scanner(request);
                
                if(!scanner.hasNextLine())
                    continue;
                
                String requestType = scanner.nextLine();
                                                
                switch(requestType) {
                    case "LOGIN USER":
                        if(scanner.hasNextLine())
                            socialNetwork.addConnectedUser(scanner.nextLine());
                        break;
                    case "LOGOUT USER":
                        if(scanner.hasNextLine())
                            socialNetwork.removeConnectedUser(scanner.nextLine());
                        break;
                    case "FRIENDSHIP REQUEST":
                        if(scanner.hasNextLine())
                            socialNetwork.newFriendshipRequest(scanner.nextLine());
                        break;
                    case "FRIENDSHIP ACCEPTED":
                        if(scanner.hasNextLine())
                            socialNetwork.friendshipAccepted(scanner.nextLine());
                        break;
                    case "NEW POST":
                        if(scanner.hasNextLine())
                            socialNetwork.addPost(scanner.nextLine());
                        break;
                    case "USER INFO":
                        String friendInfo = "";
                        int i = 4; // Number of fields related to user's info
                        while(scanner.hasNextLine() && i > 0) {
                            friendInfo += scanner.nextLine() + "\n";
                            --i;
                        }
                        socialNetwork.setFriendInfo(friendInfo);
                        // The rest of the the response is a list of songs
                        while(scanner.hasNextLine()) {
                            socialNetwork.addFriendSong(scanner.nextLine());
                        }
                        break;
                    case "PLAYING SONG":
                        if(scanner.hasNextLine())
                            playSong(Integer.parseInt(scanner.nextLine()));
                        break;
                    case "LIST POSTS":
                        socialNetwork.clearPosts();
                        while(scanner.hasNextLine())
                            socialNetwork.addPost(scanner.nextLine());
                        break;
                }
                
            }
            catch(IOException e) {
                connected = false;
            }
        }
    }

    private void playSong(int fileLength) {
            playingSong = true;
            socialNetwork.playSong();
            client.playSong(fileLength, this);
    }
    
    public synchronized void stopSong() {
        playingSong = false;
        socialNetwork.stopSong();
    }
    
    public synchronized boolean isPlayingSong() {
        return playingSong;
    }
}
