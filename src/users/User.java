package users;

import java.util.Set;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class User {
    private String username;
    private String password;
    private String placeOfBirth;
    private String dateOfBirth;
    private String residence;
    private Set<String> genres;

    public User(String username, String password, String placeOfBirth,
                String dateOfBirth, String residence, Set<String> genres) {
        this.username = username;
        this.password = password;
        this.placeOfBirth = placeOfBirth;
        this.dateOfBirth = dateOfBirth;
        this.residence = residence;
        this.genres = genres;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getResidence() {
        return residence;
    }

    public Set<String> getGenres() {
        return genres;
    }
}
