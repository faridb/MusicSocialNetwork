package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import server.gui.ChatServerUI;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class ChatServer implements Runnable {
    private int portNumber;
    private boolean running;
    private ServerSocket serverSocket;
    private HashSet<ChatHandler> connectedHandlers;
    private ChatServerUI chatServerUI;

    public ChatServer(int portNumber, ChatServerUI ui) {
        this.portNumber = portNumber;
        connectedHandlers = new HashSet<>();
        chatServerUI = ui;
        running = false;
    }
    
    private void start() {
        try {
            System.out.println("Starting chat server...");
            chatServerUI.logLine("Starting chat server...");
            serverSocket = new ServerSocket(portNumber);
            
            System.out.println("Waiting for clients...");
            chatServerUI.logLine("Waiting for clients...");
            // Wait for clients and start a thread for each accepted connection
            running = true;
            while(running) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client accepted: " + clientSocket.getInetAddress());
                chatServerUI.logLine("Client accepted: " + clientSocket.getInetAddress());
                
                ChatHandler chatHandler = new ChatHandler(this, clientSocket, chatServerUI);
                Thread t = new Thread(chatHandler);
                t.start();
            }   
        }
        catch(IOException e) {
            System.out.println("Chat Server down: " + e.getMessage());
        }
    }
    
    public void stop() {
        running = false;
        try {
            serverSocket.close();
        }
        catch(IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        }
        // Stop the chat handlers
        connectedHandlers.forEach((chatHandler) -> {
            chatHandler.stop();
        });
    }

    /**
     * Send a message to a specific user
     * @param toUsername Username of the user whom to send the message to
     * @param fromUsername Who sent the message
     * @param message Message body
     */
    public synchronized void sendMessage(String toUsername, String fromUsername, String message) {
        for(ChatHandler cHandler : connectedHandlers) {
            if(cHandler.getUsername().equals(toUsername)) {
                cHandler.sendString("SENT MESSAGE\n"
                        + fromUsername + "\n"
                        + message);
                    break;
            }
        }
    }
    
    public synchronized void removeConnectedUser(String username) {
        for(ChatHandler cHandler : connectedHandlers) {
            if(cHandler.getUsername().equals(username)) {
                connectedHandlers.remove(cHandler);
                // Broadcast the disconnected user to all the others
                String request = "LOGOUT USER\n" + username;
                broadcastString(request);
                break;
            }
        }
        chatServerUI.setClientsNb(connectedHandlers.size());
    }
    
    public synchronized void broadcastString(String string) {
        for(ChatHandler cHandler : connectedHandlers) {
            cHandler.sendString(string);
        }
    }

    public synchronized void askSendFile(String toUsername, String fromUsername,
            String fileName, String fileLength) {
        
        for(ChatHandler cHandler : connectedHandlers) {
            if(cHandler.getUsername().equals(toUsername)) {
                cHandler.askSendFile(fromUsername, fileName, fileLength);
                break;
            }
        }        
    }
    
    public synchronized void sendToUser(String string, String username) {
        for(ChatHandler aHandler : connectedHandlers) {
            if(aHandler.getUsername().equals(username)) {
                aHandler.sendString(string);
                break;
            }
        }
    }

    public synchronized void sendFile(String toUsername, String fromUsername,
            String fileName, byte[] bytes)
    {
        for(ChatHandler aHandler : connectedHandlers) {
            if(aHandler.getUsername().equals(toUsername)) {
                aHandler.receiveFile(fileName, fromUsername, bytes);
                break;
            }
        }
    }
    
    public synchronized void addConnectedUser(ChatHandler chatHandler) {
        connectedHandlers.add(chatHandler);
        chatServerUI.setClientsNb(connectedHandlers.size());
    }

    @Override
    public void run() {
        start();
    }
}
