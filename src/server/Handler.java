package server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import server.gui.ServerUI;
import users.User;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class Handler implements Runnable {
    private static final String USERS_FILE = "data" + File.separator 
            + "users.txt";
    private static final String POSTS_FILE = "data" + File.separator 
            + "posts.txt";
    public static final String ONLINE_USERS_FILE = "data" 
            + File.separator + "online-users.txt";
    private static final String SONGS_FILE = "data" 
            + File.separator + "songs.txt";
    private static final String SONGS_DIR = "data" 
            + File.separator + "songs" + File.separator;
    public static final String FIELDS_SEP = "\\";
    private static final String INNER_FIELDS_SEP = ",";
    
    private Server server;
    private Socket clientSocket;
    private DataInputStream in;
    private DataOutputStream out;
    private boolean connected;
    private User connectedUser;
    private ServerUI serverUI;

    public Handler(Server server, Socket clientSocket, ServerUI ui) {
        this.server = server;
        this.clientSocket = clientSocket;
        this.serverUI = ui;
        
        try {
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        connected = true;
    }
    
    @Override
    public void run() {
        System.out.println("Client connected: " + clientSocket.getInetAddress());
        serverUI.logLine("Client connected: " + clientSocket.getInetAddress());
        
        /* Wait for messages from the client */
        while(connected) {
            try {
                String text = in.readUTF();
                handleRequest(text);
            }
            catch(IOException e) {
                System.out.println("Client disconnected: " + clientSocket.getInetAddress());
                serverUI.logLine("Client disconnected: " + clientSocket.getInetAddress());
                connected = false;
            }
        }
    }
    
    /**
     * Handle a request given as a string according to our protocol
     * @param request A String containing a request 
     */
    private void handleRequest(String request) {
        Scanner scanner = new Scanner(request);
        
        if(!scanner.hasNextLine())
            return;
        
        String requestType = scanner.nextLine();
        String response = "";
                
        switch (requestType) {
            case "REGISTER NEW USER":
                try {
                    User user = new User(
                            scanner.nextLine(),
                            scanner.nextLine(),
                            scanner.nextLine(),
                            scanner.nextLine(),
                            scanner.nextLine(),
                            new HashSet<>(Arrays.asList(scanner.nextLine()
                                    .split(INNER_FIELDS_SEP)))
                    );
                    if(createNewUser(user)) {
                        System.out.println("Registered new user: " + user.getUsername());
                        serverUI.logLine("Registered new user: " + user.getUsername());
                    }
                    else {
                        System.err.println("Could not register new user: " + user.getUsername());
                        serverUI.logLine("Could not register new user: " + user.getUsername());
                    }
                    
                }
                catch(NoSuchElementException e) {
                    System.err.println("NoSuchElementException: " + e.getMessage());
                }   break;
            case "LOGIN USER":
                try {
                    String username = scanner.nextLine();
                    String password = scanner.nextLine();
                    
                    loginUser(username, password);
                }
                catch(NoSuchElementException e) {
                    System.err.println("NoSuchElementException: " + e.getMessage());
                }   break;
            case "LIST ONLINE USERS":
                response = "LIST ONLINE USERS";
                Set<User> connectedUsers = server.getConnectedUsers();
                for(User user : connectedUsers) {
                    response += "\n" + user.getUsername();
                }   
                sendString(response);
                serverUI.logLine("Online users list requested: " + getUser().getUsername());
                break;
            case "LOGOUT USER":
                logout();
                break;
            case "FRIENDSHIP REQUEST":
                if(scanner.hasNextLine()) {
                    String toUsername = scanner.nextLine();
                    addFriendshipRequest(toUsername);
                }
                break;
            case "LIST FRIENDSHIP REQUESTS":
                response = "LIST FRIENDSHIP REQUESTS";
                Set<String> friendshipRequests = getFriendshipRequests();
                for(String username : friendshipRequests) {
                    response += "\n" + username;
                }
                sendString(response);
                break;
            case "ACCEPT FRIENDSHIP":
                if(scanner.hasNextLine())
                    acceptFriend(scanner.nextLine());
                break;
            case "REFUSE FRIENDSHIP":
                if(scanner.hasNextLine()) 
                    refuseFriend(scanner.nextLine());
                break;
            case "LIST FRIENDS":
                response = "LIST FRIENDS";
                for(String username : getFriends()) {
                    response += "\n" + username;
                }
                sendString(response);
                break;
            case "NEW POST":
                if(scanner.hasNextLine())
                    newPost(scanner.nextLine());
                break;
            case "LIST POSTS":
                response = "LIST POSTS";
                for(String post : getPosts()) {
                    response += "\n" + post;
                }
                sendString(response);
                break;
            case "USER INFO":
                if(scanner.hasNextLine()) {
                    sendUserInfo(scanner.nextLine());
                }
                break;
            case "UPLOAD SONG":
                try {
                    downloadSong(scanner.nextLine(), scanner.nextLine(),
                            scanner.nextLine());
                }
                catch(NoSuchElementException e) {
                    System.err.println("NoSuchElementException: " + e.getMessage());
                }
                break;
            case "LIST SONGS":
                response = "LIST SONGS";
                for(String song : getSongs(getUser().getUsername())) {
                    response += "\n" + song;
                }
                sendString(response);
                break;
            case "REMOVE SONG":
                if(scanner.hasNextLine())
                    removeSong(scanner.nextLine());
                break;
            case "PLAY SONG":
                try {
                    playSong(scanner.nextLine(), scanner.nextLine());
                }
                catch(NoSuchElementException e) {
                    System.err.println("NoSuchElementException: " + e.getMessage());
                }
                break;
            default:
                System.err.println("Unknown request received");
                break;
        }
    }
    
    /**
     * Add a new user to the Server's database
     * Check if the username is not already used by another user
     * Inform the client if the user was register successfully
     * @param user A User to be added to the database
     */
    private boolean createNewUser(User user) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String line;
            
            // Check if the username is already taken
            while( (line = reader.readLine()) != null ) {
                String username = line.substring(0, line.indexOf(FIELDS_SEP));
                if(username.equals(user.getUsername())) {
                    sendString("REGISTER USERNAME TAKEN");
                    return false;
                }
            }
            reader.close();
            
            // Add the new user to the database
            String genresList = String.join(INNER_FIELDS_SEP, user.getGenres());
            String newUser = user.getUsername() + FIELDS_SEP
                    + user.getPassword() + FIELDS_SEP
                    + user.getPlaceOfBirth() + FIELDS_SEP
                    + user.getDateOfBirth() + FIELDS_SEP
                    + user.getResidence() + FIELDS_SEP
                    + genresList
                    + FIELDS_SEP + FIELDS_SEP; // Reserve fields for friends

            if(printlnToFile(USERS_FILE, newUser)) {
                sendString("REGISTER USER SUCCESS");
                // Add the user to the list of connected users
                addToOnlineUsers(user);
                return true;
            }
            else {
                sendString("ERROR");
                return false;
            }
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return false;
    }
    
    /**
     * Write a given line to a given file
     * @param file File path
     * @param line Line to be written to the file
     */
    private synchronized boolean printlnToFile(String file, String line) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(file, true))) {
            pw.println(line);
            
            return true;
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return false;
    }
    
    /**
     * Send the passed String to the Client Socket
     * @param strToSend String to be sent to the client
     */
    public void sendString(String strToSend) {
        try {
            out.writeUTF(strToSend);
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Check if the given username and password are correct
     * @param username The username
     * @param password The password
     * @return 
     */
    private boolean loginUser(String username, String password) {
        // Check if the user is already logged in
        if(server.isUserLoggedIn(username)) {
            sendString("ALREADY LOGGED IN");
            serverUI.logLine("Login failed (Already logged in): " + username);
            return false;
        }
        
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String line;
            
            // Find the user with this specific username
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP));
                
                if(fields[0].equals(username) && fields[1].equals(password)) {
                    // Send login success request including user's info
                    String loginResponse = "LOGIN SUCCESS\n"
                            + fields[0] + "\n"
                            + fields[1] + "\n"
                            + fields[2] + "\n"
                            + fields[3] + "\n"
                            + fields[4] + "\n"
                            + fields[5];
                    sendString(loginResponse);
                    
                    // Add the user to the list of connected users
                    User user = new User(
                            fields[0],
                            fields[1],
                            fields[2],
                            fields[3],
                            fields[4],
                            new HashSet<>(Arrays.asList(fields[5].
                                    split(INNER_FIELDS_SEP)))
                    );  
                    addToOnlineUsers(user);
                    
                    serverUI.logLine("User logged in: " + username);
                    
                    return true;
                }
            }
            reader.close();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        sendString("LOGIN INCORRECT");
        serverUI.logLine("Login failed (Incorrect credentials): " + username);
        return false;
    }
    
    /**
     * Remove the user from the online users list
     */
    private synchronized void logout() {
        server.removeConnectedUser(connectedUser.getUsername());
        try {
            BufferedReader reader = new BufferedReader(new FileReader(ONLINE_USERS_FILE));
            String fileContents = "";
            String line;
            
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP), -1);
                
                if(!fields[0].equals(connectedUser.getUsername())) {
                    fileContents += line + System.lineSeparator();
                }
            }
            reader.close();
            
            // Write back the file contents
            PrintWriter pw = new PrintWriter(ONLINE_USERS_FILE);
            pw.write(fileContents);
            pw.close();
            
            serverUI.logLine("User logged out: " + getUser().getUsername());
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Adds the user to the list of online users
     * @param user 
     */
    private void addToOnlineUsers(User user) {
        connectedUser = user;
        server.addConnectedUser(this);
                    
        // Add to online users file
        String onlineUser = user.getUsername() + FIELDS_SEP 
            + clientSocket.getInetAddress();
        printlnToFile(ONLINE_USERS_FILE, onlineUser);
    }
    
    /**
     * Add a new friendship request to the users database
     * @param toUsername Username of the user receiving the friendship request
     * @param friendUsername Username of the user sending the request
     */
    private synchronized boolean addFriendshipRequest(String toUsername) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String fileContents = "";
            String line;
            String request = "";
            String friendUsername = getUser().getUsername();
            
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP), -1);
                
                if(fields[0].equals(toUsername)
                        // Check if the request already exists
                        && !Arrays.asList(fields[6].split(INNER_FIELDS_SEP)).
                            contains(friendUsername)
                        // Check if already a friend
                        && !Arrays.asList(fields[7].split(INNER_FIELDS_SEP)).
                            contains(friendUsername)) {
                    fields[6] += (fields[6].isEmpty()) ? friendUsername 
                            : INNER_FIELDS_SEP + friendUsername;
                    line = String.join(FIELDS_SEP, fields);
                    
                    // Send a friendship request notification
                    request = "FRIENDSHIP REQUEST\n"
                        + friendUsername;
                    server.sendToUser(request, toUsername);
                }
                fileContents += line + System.lineSeparator();
            }
            reader.close();
            
            // Write back the file contents
            PrintWriter pw = new PrintWriter(USERS_FILE);
            pw.write(fileContents);
            pw.close();
            
            serverUI.logLine("Friendship request: " + getUser().getUsername() +
                    " -> " + toUsername);
            
            return true;
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return false;
    }
    
    /**
     * Get a list of the friendship requests of the current user from the database
     * @return List of friendship requests
     */
    private Set<String> getFriendshipRequests() {
        Set<String> friendshipRequests = new HashSet<>();            
        
        // Get the list of friendship requests from the database
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String line;
            
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP), -1);
                
                if(fields[0].equals(connectedUser.getUsername())) {
                    friendshipRequests = new HashSet<>(Arrays.asList(
                            fields[6].split(INNER_FIELDS_SEP)));
                    
                    return friendshipRequests;
                }
            }
            
            reader.close();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return friendshipRequests;
    }
    
    /**
     * Get the list of friends of the current user
     * @return 
     */
    private HashSet<String> getFriends() {
        HashSet<String> friends = new HashSet<>();            
        
        // Get the list of friendship requests from the database
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String line;
            
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP), -1);
                
                if(fields[0].equals(connectedUser.getUsername())) {
                    friends = new HashSet<>(Arrays.asList(
                            fields[7].split(INNER_FIELDS_SEP)));
                    
                    return friends;
                }
            }
            
            reader.close();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return friends;
    }
    
    /**
     * Add the giver username as a friend
     * @param username 
     */
    private synchronized void acceptFriend(String friendUsername) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String fileContents = "";
            String line;
            String toUsername = getUser().getUsername();
            String request = "";
            
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP), -1);
                
                if(fields[0].equals(toUsername)) {
                    // Remove from the friends requests
                    LinkedList<String> frRequests = new LinkedList<>(
                            Arrays.asList(fields[6].split(INNER_FIELDS_SEP)));
                    frRequests.remove(friendUsername);
                    fields[6] = String.join(INNER_FIELDS_SEP, frRequests);

                    // Add to the list of friends if not already there
                    if(!Arrays.asList(fields[7].split(INNER_FIELDS_SEP)).
                            contains(friendUsername)) {
                        fields[7] += (fields[7].isEmpty()) ? friendUsername 
                                : INNER_FIELDS_SEP + friendUsername;
                        
                        // Send a friendship accepted notification
                        request = "FRIENDSHIP ACCEPTED\n"
                                + toUsername;
                        server.sendToUser(request, friendUsername);
                    }
                    line = String.join(FIELDS_SEP, fields);
                }
                else if(fields[0].equals(friendUsername)) {
                    // Remove from the friends requests
                    LinkedList<String> frRequests = new LinkedList<>(
                            Arrays.asList(fields[6].split(INNER_FIELDS_SEP)));
                    frRequests.remove(toUsername);
                    fields[6] = String.join(INNER_FIELDS_SEP, frRequests);

                    // Add to the list of friends if not already there
                    if(!Arrays.asList(fields[7].split(INNER_FIELDS_SEP)).
                            contains(toUsername)) {
                        fields[7] += (fields[7].isEmpty()) ? toUsername 
                                : INNER_FIELDS_SEP + toUsername;
                    }
                    line = String.join(FIELDS_SEP, fields);
                }
                fileContents += line + System.lineSeparator();
            }
            reader.close();
            
            // Write back the file contents
            PrintWriter pw = new PrintWriter(USERS_FILE);
            pw.write(fileContents);
            pw.close();
            
            serverUI.logLine("Friendship request accepted: " 
                    + getUser().getUsername() + " -> " + friendUsername);
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Remove the given username from the friendship requests list
     * @param friendUsername Username to remove
     */
    private synchronized void refuseFriend(String friendUsername) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String fileContents = "";
            String line;
            String toUsername = getUser().getUsername();
            
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP), -1);
                
                if(fields[0].equals(toUsername)) {
                    // Remove from the friends requests
                    LinkedList<String> frRequests = new LinkedList<>(
                            Arrays.asList(fields[6].split(INNER_FIELDS_SEP)));
                    frRequests.remove(friendUsername);
                    fields[6] = String.join(INNER_FIELDS_SEP, frRequests);

                    line = String.join(FIELDS_SEP, fields);
                }
                fileContents += line + System.lineSeparator();
            }
            reader.close();
            
            // Write back the file contents
            PrintWriter pw = new PrintWriter(USERS_FILE);
            pw.write(fileContents);
            pw.close();
            
            serverUI.logLine("Friendship request refused: " 
                    + getUser().getUsername() + " -> " + friendUsername);
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    /**
     * Create a new post and notify online friends
     * @param post Contents of the post
     */
    private void newPost(String post) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");
        LocalDateTime dateTime = LocalDateTime.now();
        String newPost = getUser().getUsername() + FIELDS_SEP 
                + dateTime.format(formatter) + FIELDS_SEP
                + post;
        printlnToFile(POSTS_FILE, newPost);
        
        // Notify online friends of the new post
        HashSet<String> friends = getFriends();
        String request = "NEW POST\n"
                + "(" + dateTime.format(formatter) + ") " 
                + getUser().getUsername() 
                + ": " + post;
        server.sendToUsers(request, friends);
        
        serverUI.logLine("New post from: " + getUser().getUsername());
    }
    
    /**
     * Get the posts of the current user and his/her friends
     * @return ArrayList containing a list of posts
     */
    private ArrayList<String> getPosts() {
        ArrayList<String> posts = new ArrayList<>();
        Set<String> users = getFriends();
        users.add(getUser().getUsername());
        
        try {
            BufferedReader reader = new BufferedReader(new FileReader(POSTS_FILE));
            String line;
            
            // Find the user with this specific username
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP));
                
                for(String username : users) {
                    if(fields[0].equals(username)) {
                        posts.add("(" + fields[1] + ") " + fields[0] + ": " + fields[2]);
                        break;
                    }
                }
            }
            reader.close();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return posts;
    }
    
    /**
     * Send the info of the given user to the client
     * @param username 
     */
    private void sendUserInfo(String username) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(USERS_FILE));
            String line;
            String response = "";
            
            // Find the user with this specific username
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP));
                
                if(fields[0].equals(username)) {
                    response = "USER INFO\n"
                            + "Place of Birth: " + fields[2] + "\n"
                            + "Date of Birth: " + fields[3] + "\n"
                            + "Residence: " + fields[4] + "\n"
                            + "Genres: " + fields[5];
                    break;
                }
            }         
            reader.close();
            // Send the list of songs shared by this user
            for(String song : getSongs(username)) {
                response += "\n" + song;
            }
            sendString(response);
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Get the list of the songs shared by a user
     * @param username
     * @return ArrayList of the songs
     */
    private ArrayList<String> getSongs(String username) {
        ArrayList<String> songs = new ArrayList<>();
        
        try {
            BufferedReader reader = new BufferedReader(new FileReader(SONGS_FILE));
            String line;
            
            // Find the user with this specific username
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP));
                
                if(fields[0].equals(username)) {
                    songs.add(fields[1] + " (" + fields[2] + ")");
                }
            }
            reader.close();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
        return songs;
    }
    
    public User getUser() {
        return connectedUser;
    }

    /**
     * Download a song from a client and add it to the database
     * @param title Title of the song
     * @param artist Artist(s) of the song
     */
    private void downloadSong(String title, String artist, String strLength) {
        int fileLength = Integer.parseInt(strLength);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH-mm-ss_dd-MM-yy");
        LocalDateTime date = LocalDateTime.now();
        
        String fileName = getUser().getUsername() + "_" + title 
                + "_" + artist + "_" + formatter.format(date) + ".wav";
        String filePath = SONGS_DIR + fileName;
        
        try (BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(filePath))) {
            byte[] data = new byte[fileLength];

            int remaining = fileLength;
            while(remaining > 0) {
                int read = in.read(data);
                remaining -= read;
                bos.write(data, 0, read);
            }
            
            // Add the song to the database
            String newSong = getUser().getUsername() + FIELDS_SEP + title
                    + FIELDS_SEP + artist + FIELDS_SEP + fileName;
            printlnToFile(SONGS_FILE, newSong);
            
            serverUI.logLine("Song upload: " 
                    + getUser().getUsername() + " - " + title + " (" + artist
                    + ")");
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    /**
     * Remove the song from the user's shared songs list
     * @param songStr 
     */
    private synchronized void removeSong(String songStr) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(SONGS_FILE));
            String fileContents = "";
            String line;
            
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP), -1);
                
                if(fields[0].equals(getUser().getUsername())) {
                    if(songStr.equals(fields[1] + " (" + fields[2] + ")")) {
                        Files.deleteIfExists(Paths.get(SONGS_DIR + fields[3]));
                        continue;
                    }
                }
                
                fileContents += line + System.lineSeparator();
            }
            reader.close();
            
            // Write back the file contents
            PrintWriter pw = new PrintWriter(SONGS_FILE);
            pw.write(fileContents);
            pw.close();
            
            serverUI.logLine("Song removed: " + getUser().getUsername() 
                    + " - " + songStr);
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    private void playSong(String username, String songStr) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(SONGS_FILE));
            String line;
            String fileName = "";
            
            // Find the user with this specific username
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(FIELDS_SEP));
                
                if(fields[0].equals(username)) {
                    if(songStr.equals(fields[1] + " (" + fields[2] + ")")) {
                        fileName = fields[3];
                        break;
                    }
                }
            }         
            reader.close();
            streamSong(fileName);
            
            serverUI.logLine("Playing song: " + getUser().getUsername()
                    + " - " + songStr);
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Stream a music file to the user
     * @param fileName 
     */
    private void streamSong(String fileName) {
        File file = new File(SONGS_DIR + fileName);

        try {
            BufferedInputStream bis = new BufferedInputStream(
                    new FileInputStream(file));
            byte[] data = new byte[(int) file.length()];
            
            sendString("PLAYING SONG\n"
                + file.length());

            bis.read(data);
            out.write(data);
            out.flush();
        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    /**
     *  Stop the handler thread
     */
    public void stop() {
        connected = false;
        try {
            clientSocket.close();
        }
        catch(IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        }    
    }
}
