package server;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;
import server.gui.ChatServerUI;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class ChatHandler implements Runnable {
    private ChatServer chatServer;
    private Socket clientSocket;
    private DataInputStream in;
    private DataOutputStream out;
    private boolean connected;
    private String username;
    private ChatServerUI chatServerUI;
    
    public ChatHandler(ChatServer chatServer, Socket clientSocket, ChatServerUI ui) {
        this.chatServer = chatServer;
        this.clientSocket = clientSocket;
        this.chatServerUI = ui;
        
        try {
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        connected = true;
    }
    
    @Override
    public void run() {
        System.out.println("Client connected: " + clientSocket.getInetAddress());
        chatServerUI.logLine("Client connected: " + clientSocket.getInetAddress());
        
        /* Wait for messages from the client */
        while(connected) {
            try {
                String text = in.readUTF();
                handleRequest(text);
            }
            catch(IOException e) {
                System.out.println("Client disconnected: " + clientSocket.getInetAddress());
                chatServerUI.logLine("Client disconnected: " + clientSocket.getInetAddress());
                connected = false;
            }
        }
    }
    
    /**
     * Send the passed String to the Client Socket
     * @param strToSend String to be sent to the client
     */
    public void sendString(String strToSend) {
        try {
            out.writeUTF(strToSend);
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }
    
    /**
     * Wait to receive a string from the Client
     * @return
     * @throws IOException 
     */
    public String receiveString() throws IOException {
        return in.readUTF();
    }
    
    /**
     * Handle a request given as a string according to our protocol
     * @param request A String containing a request 
     */
    private void handleRequest(String request) {
        Scanner scanner = new Scanner(request);
        
        if(!scanner.hasNextLine())
            return;
        
        String requestType = scanner.nextLine();
        
        switch (requestType) {
            case "LOGIN USER":
                if(scanner.hasNextLine())
                    loginUser(scanner.nextLine());
                break;
            case "LOGOUT USER":
                if(username != null) {
                    chatServer.removeConnectedUser(username);
                    chatServerUI.logLine("User logged out: " + username);
                }
                break;
            case "SEND MESSAGE":
                try {
                    sendMessage(scanner.nextLine(), scanner.nextLine());
                }
                catch(NoSuchElementException e) {
                    System.err.println("NoSuchElementException: " + e.getMessage());
                } 
                break;
            case "SEND FILE ?":
                try {
                    sendFileRequest(scanner.nextLine(), scanner.nextLine(),
                            scanner.nextLine());
                }
                catch(NoSuchElementException e) {
                    System.err.println("NoSuchElementException: " + e.getMessage());
                } 
                break;
            case "SEND FILE YES":
                if(scanner.hasNextLine())
                    acceptFile(scanner.nextLine());
                break;
            case "SEND FILE NO":
                if(scanner.hasNextLine())
                    refuseFile(scanner.nextLine());
                break;
            case "SEND FILE FOR REAL":
                try {
                    sendFile(scanner.nextLine(), scanner.nextLine(),
                            scanner.nextLine());
                }
                catch(NoSuchElementException e) {
                    System.err.println("NoSuchElementException: " + e.getMessage());
                }
                break;
            default:
                System.err.println("Unknown request received");
                break;
        }
    }

    /**
     * Send a message to a specific user
     * @param toUsername Username of the user whom to send the message to
     * @param message Message body
     */
    private void sendMessage(String toUsername, String message) {
        chatServer.sendMessage(toUsername, username, message);
        chatServerUI.logLine("Message sent: " + username + " -> " + toUsername);
    }
    
    public void askSendFile(String username, String fileName, String fileLength) {
        sendString("SEND FILE ?\n" + username + "\n" + fileName + "\n"
                + fileLength);
    }
    
    /**
     * Send a file to a user
     * @param toUsername
     * @param fileName
     * @param fileLength 
     */
    private void sendFileRequest(String toUsername, String fileName, String fileLength) {
        chatServer.askSendFile(toUsername, getUsername(), fileName, fileLength);
        chatServerUI.logLine("File transfer request: (" + fileName + ") "
                + getUsername() + " -> " + toUsername);
    }
    
    public String getUsername() {
        return username;
    }

    private void refuseFile(String username) {
        chatServer.sendToUser("SEND FILE NO\n" + getUsername(), username);
        chatServerUI.logLine("File transfer refused: "
                + getUsername() + " -> " + username);
    }

    private void acceptFile(String username) {
        chatServer.sendToUser("SEND FILE YES\n" + getUsername(), username);
        chatServerUI.logLine("File transfer accepted: "
                + getUsername() + " -> " + username);
    }

    private void sendFile(String toUsername, String fileName, String fileLengthStr) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int fileLength = Integer.parseInt(fileLengthStr);
        try (BufferedOutputStream bos = new BufferedOutputStream(byteStream)) {
            byte[] data = new byte[fileLength];

            int remaining = fileLength;
            while(remaining > 0) {
                int read = in.read(data);
                remaining -= read;
                bos.write(data, 0, read);
            }
            // Send the file to the user
            chatServer.sendFile(toUsername, getUsername(), fileName,
                    byteStream.toByteArray());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    public void receiveFile(String fileName, String fromUsername, byte[] data) {
        sendString("RECEIVE SENT FILE\n" + fromUsername 
                + "\n" + fileName + "\n" + data.length);
        
        try {
            out.write(data);
            out.flush();
            
            chatServerUI.logLine("File received: (fileName) "
                + fromUsername + " -> " + getUsername());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    private synchronized void loginUser(String username) {
        try {
            BufferedReader reader = new BufferedReader(
                    new FileReader(Handler.ONLINE_USERS_FILE));
            String line;
           
            // Check if the user is actually logged in the Server
            while( (line = reader.readLine()) != null ) {
                String[] fields = line.split(Pattern.quote(Handler.FIELDS_SEP));
                if(fields[0].equals(username)
                        && fields[1].equals(clientSocket.getInetAddress().toString())) {
                    this.username = username;
                    chatServer.addConnectedUser(this);
                    chatServerUI.logLine("User logged in: " + username);
                    break;
                }
            }         
            reader.close();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
        catch(IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        
    }
    
    /**
     * Stop the chat handler thread
     */
    public void stop() {
        connected = false;
        try {
            clientSocket.close();
        }
        catch(IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        }   
    }
}
