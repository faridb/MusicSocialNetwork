package server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import server.gui.ServerUI;
import users.User;

/**
 *
 * @author Farid Boudedja (T0073322)
 */
public class Server implements Runnable {
    private int portNumber;
    private boolean running;
    private ServerSocket serverSocket;
    private HashSet<Handler> connectedHandlers;
    private ServerUI serverUI;

    public Server(int portNumber, ServerUI ui) {
        this.portNumber = portNumber;
        connectedHandlers = new HashSet<>();
        serverUI = ui;
        
        // Clear online users file
        try {
            new PrintWriter(Handler.ONLINE_USERS_FILE).close();
        }
        catch(FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
        }
    }
    
    private void start() {
        try {
            System.out.println("Starting server...");
            serverUI.logLine("Starting server...");
            serverSocket = new ServerSocket(portNumber);

            System.out.println("Waiting for clients...");
            serverUI.logLine("Waiting for clients...");
            // Wait for clients and start a thread for each accepted connection
            running = true;
            while(running) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client accepted: " + clientSocket.getInetAddress());
                serverUI.logLine("Client accepted: " + clientSocket.getInetAddress());
                
                Thread t = new Thread(new Handler(this, clientSocket, serverUI));
                t.start();
            }
            
        }
        catch(IOException e) {
            System.out.println("Server down: " + e.getMessage());
        }
    }
    
    public void stop() {
        running = false;
        try {
            serverSocket.close();
        }
        catch(IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        }
        // Stop the handlers
        connectedHandlers.forEach((handler) -> {
            handler.stop();
        });
    }
    
    public synchronized void addConnectedUser(Handler handler) {
        // Broadcast the newly connected users to all the others
        String request = "LOGIN USER\n" + handler.getUser().getUsername();
        broadcastString(request);
        // Add the newly connected user to the list of online clients
        connectedHandlers.add(handler);
        serverUI.setClientsNb(connectedHandlers.size());
    }
    
    public synchronized void removeConnectedUser(String username) {
        for(Handler aHandler : connectedHandlers) {
            if(aHandler.getUser().getUsername().equals(username)) {
                connectedHandlers.remove(aHandler);
                // Broadcast the disconnected user to all the others
                String request = "LOGOUT USER\n" + username;
                broadcastString(request);
                break;
            }
        }    
        serverUI.setClientsNb(connectedHandlers.size());
    }
    
    public synchronized HashSet<User> getConnectedUsers() {
        HashSet<User> onlineUsers = new HashSet<>();
        for(Handler aHandler : connectedHandlers) {
            onlineUsers.add(aHandler.getUser());
        }
        return onlineUsers;
    }
    
    public synchronized void broadcastString(String string) {
        for(Handler aHandler : connectedHandlers) {
            aHandler.sendString(string);
        }
    }
    
    public synchronized void sendToUser(String string, String username) {
        for(Handler aHandler : connectedHandlers) {
            if(aHandler.getUser().getUsername().equals(username)) {
                aHandler.sendString(string);
                break;
            }
        }
    }
    
    public synchronized void sendToUsers(String string, HashSet<String> usernames) {
        for(Handler aHandler : connectedHandlers) {
            for(String username : usernames) {
                if(aHandler.getUser().getUsername().equals(username)) {
                    aHandler.sendString(string);
                    break;
                }
            }
        }
    }
    
    public synchronized boolean isUserLoggedIn(String username) {
        for(Handler aHandler : connectedHandlers) {
            if(aHandler.getUser().getUsername().equals(username))
                return true;
        }
        
        return false;
    }

    @Override
    public void run() {
        start();
    }
    
    public int getClientsNumber() {
        return connectedHandlers.size();
    }
}
